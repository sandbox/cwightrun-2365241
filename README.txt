
The TweetWall Module provides a custom block for placement of the TweetWall
functionality into Drupal. This is ideal for if a site needs a TweetWall
integration.

Usage
-----
The TweetWall module creates a new block under /admin/structure/block called
"TweetWall" which allows for configuring of the TweetWall options such as
Header, Border, Border Color, Background Color, Width (px or %) and Height
(px or %).

Acknowledgements 
----------------
This module's initial development was sponsored by WestEd (http://ww.wested.org)
and ongoing maintenance is sponsored by Chapter Three
(http://www.chapterthree.com).

Authors
-------
Casey Wight - https://www.drupal.org/u/cwightrun
